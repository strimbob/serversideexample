var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var webpack = require('webpack')
var isProd = process.env.NODE_ENV === 'production' // true or false
module.exports = {
  devtool: 'inline-source-map',
  // devtool: 'cheap-module-source-map',
  entry: './src/index.js',


  resolve:{
  extensions: [' ', '.js', '.jsx'],
     modules: [
         'node_modules'
     ]
},
  module:{
    loaders:[
      {test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader'},
      {test: /\.js|jsx$/, loader:'babel-loader', exclude: /node_modules/},
      { test: /\.(jpe?g|png|gif)$/i,
  loaders: ['file-loader?name=[path][name].[ext]&outputPath=images/', {
  loader: 'image-webpack-loader',
  query: {mozjpeg: {progressive: true,},
      gifsicle: {interlaced: false,},
      optipng: {optimizationLevel: 4,},
      pngquant: {quality: '75-90',speed: 3,},},}],
  exclude: /node_modules/,include: __dirname,}
    ]
  },
  devServer: {
  contentBase: path.join(__dirname, 'public'),
  compress: true,
  hot: true,
  open: true
},
  output:{
    filename: 'bundle.js',
    path: __dirname + '/public'
  },
  plugins: [
  new HtmlWebpackPlugin({
    title: 'MegaBenZine',
    hash: true,
    template: './src/html/index.html',
    minify   : {
html5                          : true,
collapseWhitespace             : true,
minifyCSS                      : true,
minifyJS                       : true,
minifyURLs                     : false,
removeAttributeQuotes          : true,
removeComments                 : true,
removeEmptyAttributes          : true,
removeOptionalTags             : true,
removeRedundantAttributes      : true,
removeScriptTypeAttributes     : true,
removeStyleLinkTypeAttributese : true,
useShortDoctype                : true
}
  }),
  new ExtractTextPlugin({
    filename: 'app.css',
    disable: !isProd,
    allChunks: true
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin()
]
};
